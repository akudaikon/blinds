Import("env")
import os
import subprocess

def makeLink(source, target, env):
    sourceFile = os.path.abspath(str(target[0]))
    linkFile = os.path.abspath(target[0].name)
    if os.path.exists(linkFile): os.remove(linkFile)
    subprocess.call(["mklink", "/H", linkFile, sourceFile], shell=True)

env.AddPostAction("$BUILD_DIR/firmware.bin", makeLink)
env.AddPostAction("$BUILD_DIR/spiffs.bin", makeLink)
