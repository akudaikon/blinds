var ipAddress = document.getElementById('ip'),
    fullStepRange = document.getElementById('fullStepRange'),
    motorInvert = document.getElementById('motorInvert'),
    mdnsName = document.getElementById('mdnsName'),
    mqttServer = document.getElementById('mqttServer'),
    mqttUser = document.getElementById('mqttUser'),
    mqttPassword = document.getElementById('mqttPassword')
    mqttCommandTopic = document.getElementById('mqttCommandTopic'),
    mqttStateTopic = document.getElementById('mqttStateTopic'),
    mqttTiltCommandTopic = document.getElementById('mqttTiltCommandTopic'),
    mqttTiltStateTopic = document.getElementById('mqttTiltStateTopic');

getSettings = function()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        var settings = xhttp.responseText.split(",");
        ipAddress.innerHTML = "<h6>" + settings[2] + "</h6>";
        fullStepRange.innerHTML = "<h6>" + settings[7] + "</h6>";
        motorInvert.checked = (settings[6] == "1" ? true : false);
        mdnsName.value = settings[0];
        mqttServer.value = settings[3];
        mqttUser.value = settings[4];
        mqttPassword.value = settings[5];
        mqttCommandTopic.innerHTML = "<h6>blinds/" + settings[0] + "/command</h6>";
        mqttStateTopic.innerHTML = "<h6>blinds/" + settings[0] + "/state</h6>";
        mqttTiltCommandTopic.innerHTML = "<h6>blinds/" + settings[0] + "/tilt</h6>";
        mqttTiltStateTopic.innerHTML = "<h6>blinds/" + settings[0] + "/tilt-state</h6>";
      }
    }
  };
  xhttp.open("GET", "getsettings", true);
  xhttp.send();
}

// To get around onLoad not firing on back
setTimeout(getSettings, 100);