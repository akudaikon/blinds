#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServerSPIFFS.h>
#include <WiFiManager.h>
#include <DNSServer.h>
#include <WiFiClient.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <EEPROM.h>
#include <FS.h>
#include <Ticker.h>
#include <JC_Button.h>

// 200 steps per rev @ 1/8 microstepping = 1600 steps per rev

#define MOTOR_EN_PIN      16  // D0 - active high (pulled high through 470 and led)
#define MOTOR_DIR_PIN     5   // D1
#define MOTOR_STEP_PIN    4   // D2 - positive edge triggered

#define LED_PIN           2   // D4 - active low, NEED TO BE PULLED HIGH AT RESET (pulled high through 12k)

#define HALL1_PIN         12  // D6 - active low
#define HALL2_PIN         14  // D5 - active low

#define BUTTON1_PIN       13  // D7 - active high
#define BUTTON2_PIN       15  // D8 - active high, NEED TO BE PULLED DOWN AT RESET (pulled low through 12k)

#define MOTOR_STEP_DELAY            1000  // us
#define MOTOR_MAX_STEPS             7700  // steps
#define MOTOR_MAX_STEPS_TOLERANCE   400   // steps

#define SETTINGS_REV        0xA1
#define MAX_STRING_LENGTH   128

enum EEPROMSettings
{
  SETTING_INITIALIZED,
  SETTING_MOTOR_INVERT,
  SETTING_DELIM,
  // Max 128 characters for EEPROM stored strings below...
  SETTING_MDNS_LENGTH,
  SETTING_MDNS,
  SETTING_MQTT_SERVER_LENGTH = SETTING_MDNS + MAX_STRING_LENGTH,
  SETTING_MQTT_SERVER,
  SETTING_MQTT_USER_LENGTH = SETTING_MQTT_SERVER + MAX_STRING_LENGTH,
  SETTING_MQTT_USER,
  SETTING_MQTT_PASSWORD_LENGTH = SETTING_MQTT_USER + MAX_STRING_LENGTH,
  SETTING_MQTT_PASSWORD,
  NUM_OF_SETTINGS = SETTING_MQTT_PASSWORD + MAX_STRING_LENGTH
};

MDNSResponder mdns;
WiFiClient mqttClient;
PubSubClient mqtt(mqttClient);
ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;

Ticker blinkLED_tckr;
Button button1(BUTTON1_PIN, 25, false, false);
Button button2(BUTTON2_PIN, 25, false, false);

bool motorInvert = false;
String mdnsName;
String mqttServer;
String mqttUser;
String mqttPassword;

bool isMoving = false;
bool isCalibrated = false;
uint32_t currentStepCount = 0;
uint32_t targetStepCount = 0;
uint32_t fullStepCount = 0;

bool bothButtonsPressed = false;
uint32_t pressStartMillis = 0;
uint32_t lastMicros = 0;

void setup()
{
  Serial.begin(115200);

  // Initialize pins
  button1.begin();
  button2.begin();
  pinMode(MOTOR_EN_PIN, OUTPUT);
  pinMode(MOTOR_STEP_PIN, OUTPUT);
  pinMode(MOTOR_DIR_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  pinMode(HALL1_PIN, INPUT);
  pinMode(HALL2_PIN, INPUT);

  digitalWrite(MOTOR_EN_PIN, LOW);
  digitalWrite(LED_PIN, HIGH);

  // Get MAC address and build AP SSID
  uint8_t macAddr[6];
  char apName[14];
  WiFi.macAddress(macAddr);
  sprintf(apName, "Blinds-%02x%02x%02x", macAddr[3], macAddr[4], macAddr[5]);
  mdnsName = apName;

  // Initialize and read EEPROM settings
  EEPROM.begin(NUM_OF_SETTINGS);
  readEEPROMSettings();

  // Initialize SPIFFS
  SPIFFS.begin();

  // Connect to WiFi
  WiFiManager wifiManager;
  if (!wifiManager.autoConnect(apName)) ESP.reset();

  // Start servers
  mdns.begin(mdnsName.c_str());
  server.on("/", HTTP_GET, []() { handleFileRead("/config.htm"); });
  server.on("/settings", HTTP_POST, saveSettings);
  server.on("/getsettings", getSettings);
  server.on("/recalibrate", []()
  {
    server.send(200, "text/html", F("<META http-equiv=\"refresh\" content=\"10;URL='/'\">Recalibrating..."));
    calibrateRange();
  });
  server.on("/reset", [&wifiManager]()
  {
    wifiManager.resetSettings();
    server.send(200, "text/html", F("Clearing all saved settings (including Wifi) and restarting..."));
    server.handleClient();
    ESP.restart();
  });
  server.on("/restart", []()
  {
    server.send(200, "text/html", F("<META http-equiv=\"refresh\" content=\"10;URL='/'\">Restarting..."));
    server.handleClient();
    ESP.restart();
  });
  server.onNotFound([]()
  {
    if (!handleFileRead(server.uri())) server.send(404, "text/plain", "Not Found");
  });
  httpUpdater.setup(&server);
  server.begin();

  // Setup MQTT
  mqtt.setServer(mqttServer.c_str(), 1883);
  mqtt.setCallback(mqttCallback);
  mqttReconnect();

  // Calibrate stepper motor range
  calibrateRange();
}

void loop()
{
  // Handle MQTT, if enabled
  if (!mqtt.connected()) mqttReconnect();
  if (mqtt.connected()) mqtt.loop();

  // Handle HTTP
  server.handleClient();

  if (micros() - lastMicros > MOTOR_STEP_DELAY)
  {
    // Read button states
    button1.read();
    button2.read();

    // Handle button presses
    if (button1.isPressed() && button2.isPressed()) 
    {
      if (!bothButtonsPressed)
      {
        pressStartMillis = millis();
        bothButtonsPressed = true;
      }
      if (millis() - pressStartMillis > 3000) calibrateRange();
    }
    else
    {
      bothButtonsPressed = false;
      if (button1.isPressed() && isCalibrated) if (targetStepCount < fullStepCount) targetStepCount++;
      if (button2.isPressed() && isCalibrated) if (targetStepCount > 0) targetStepCount--;
    }

    // Step motor, if needed
    if (targetStepCount > currentStepCount)
    {
      if (!isMoving)
      {
        digitalWrite(MOTOR_EN_PIN, HIGH);
        digitalWrite(LED_PIN, LOW);
        isMoving = true;
      }

      if (digitalRead((motorInvert ? HALL1_PIN : HALL2_PIN)) != LOW)
      {
        digitalWrite(MOTOR_DIR_PIN, (motorInvert ? LOW : HIGH));
        digitalWrite(MOTOR_STEP_PIN, HIGH);
        digitalWrite(MOTOR_STEP_PIN, LOW);
        currentStepCount++;
      }
      else targetStepCount = currentStepCount;
    }
    else if (targetStepCount < currentStepCount)
    {
      if (!isMoving)
      {
        digitalWrite(MOTOR_EN_PIN, HIGH);
        digitalWrite(LED_PIN, LOW);
        isMoving = true;
      }

      if (digitalRead((motorInvert ? HALL2_PIN : HALL1_PIN)) != LOW)
      {
        digitalWrite(MOTOR_DIR_PIN, (motorInvert ? HIGH : LOW));
        digitalWrite(MOTOR_STEP_PIN, HIGH);
        digitalWrite(MOTOR_STEP_PIN, LOW);
        currentStepCount--;
      }
      else targetStepCount = currentStepCount;
    }
    else
    {
      if (isMoving)
      {
        digitalWrite(MOTOR_EN_PIN, LOW);
        digitalWrite(LED_PIN, HIGH);
        mqttSendUpdate();
        isMoving = false;
      }
    }
    lastMicros = micros();
  }
}

void goHome()
{
  Serial.println("Going home...");

  uint32_t stepCount = 0;
  digitalWrite(LED_PIN, LOW);
  digitalWrite(MOTOR_EN_PIN, HIGH);
  digitalWrite(MOTOR_DIR_PIN, (motorInvert ? HIGH : LOW));

  // Step backwards until we find the home position
  while(1)
  {
    if (digitalRead((motorInvert ? HALL2_PIN : HALL1_PIN)) == LOW) break;

    if (stepCount > currentStepCount + MOTOR_MAX_STEPS_TOLERANCE)
    {
      digitalWrite(MOTOR_EN_PIN, LOW);
      blinkLED_tckr.attach_ms(1000, blinkLED);
      isCalibrated = false;
      Serial.println("Failed to find home!");
      return;
    }

    digitalWrite(MOTOR_STEP_PIN, HIGH);
    digitalWrite(MOTOR_STEP_PIN, LOW);
    delayMicroseconds(MOTOR_STEP_DELAY);
    stepCount++;
    yield();
  }

  digitalWrite(LED_PIN, HIGH);
  digitalWrite(MOTOR_EN_PIN, LOW);

  targetStepCount = 0;
  currentStepCount = 0;

  Serial.println("Home found!");
}

void calibrateRange()
{
  Serial.println("Calibrating stepper range...");

  blinkLED_tckr.detach();
  blinkLED_tckr.attach_ms(250, blinkLED);

  isCalibrated = false;
  fullStepCount = 0;
  uint32_t stepCount = 0;
  digitalWrite(MOTOR_EN_PIN, HIGH);

  // Start stepping forward until we find the end
  digitalWrite(MOTOR_DIR_PIN, (motorInvert ? LOW : HIGH));
  while(1)
  {
    if (digitalRead((motorInvert ? HALL1_PIN : HALL2_PIN)) == LOW) break;

    if (stepCount > MOTOR_MAX_STEPS)
    {
      digitalWrite(MOTOR_EN_PIN, LOW);
      blinkLED_tckr.detach();
      blinkLED_tckr.attach_ms(1000, blinkLED);
      isCalibrated = false;
      Serial.println("Failed to activate foward hall in time!");
      return;
    }

    digitalWrite(MOTOR_STEP_PIN, HIGH);
    digitalWrite(MOTOR_STEP_PIN, LOW);
    delayMicroseconds(MOTOR_STEP_DELAY);
    stepCount++;
    yield();
  }

  stepCount = 0;

  // Step backwards until we find the home position
  digitalWrite(MOTOR_DIR_PIN, (motorInvert ? HIGH : LOW));
  while(1)
  {
    if (stepCount > MOTOR_MAX_STEPS)
    {
      digitalWrite(MOTOR_EN_PIN, LOW);
      blinkLED_tckr.detach();
      blinkLED_tckr.attach_ms(1000, blinkLED);
      isCalibrated = false;
      Serial.println("Failed to activate reverse hall in time!");
      return;
    }

    if (digitalRead((motorInvert ? HALL2_PIN : HALL1_PIN)) == LOW) break;
    digitalWrite(MOTOR_STEP_PIN, HIGH);
    digitalWrite(MOTOR_STEP_PIN, LOW);
    delayMicroseconds(MOTOR_STEP_DELAY);
    stepCount++;
    fullStepCount++;
    yield();
  }

  blinkLED_tckr.detach();
  digitalWrite(MOTOR_EN_PIN, LOW);
  digitalWrite(LED_PIN, HIGH);
  isCalibrated = true;
  targetStepCount = 0;
  currentStepCount = 0;
  mqttSendUpdate();

  Serial.print("Stepper range calibration complete! ");
  Serial.println(fullStepCount);
}

void blinkLED() 
{ 
  static bool ledState = false;
  digitalWrite(LED_PIN, ledState); 
  ledState = !ledState;
}

void MQTT_haDiscovery()
{
  String id;
  String dev;
  String topic;

  char buf[7];
  uint8_t macAddr[6];
  WiFi.macAddress(macAddr);
  sprintf(buf, "%02x%02x%02x", macAddr[3], macAddr[4], macAddr[5]);
  id = buf;

  dev = "\"dev\": { \"ids\": \"" + id + "\", \"name\": \"Blinds\", \"mdl\": \"Blinds\", \"mf\": \"Akudaikon\" }";

  String commandTopic;
  String stateTopic;
  String tiltCommandTopic;
  String tiltStateTopic;
  String config;
  String availTopic;

  topic = "homeassistant/cover/blinds_" + id + "/config";
  
  availTopic = "blinds/" + mdnsName + "/availability";
  commandTopic = "blinds/" + mdnsName + "/command";
  stateTopic = "blinds/" + mdnsName + "/state";
  tiltCommandTopic = "blinds/" + mdnsName + "/tilt";
  tiltStateTopic = "blinds/" + mdnsName + "/tilt-state";
  
  config = "{\"name\": \"Blinds\", \"obj_id\": \"" + mdnsName + "\", \"cmd_t\": \"" + commandTopic + "\", \"stat_t\": \"" + stateTopic + "\", \"tilt_cmd_t\": \"" + tiltCommandTopic + "\", \"tilt_status_t\": \"" + tiltStateTopic + "\", \"avty_t\": \"" + availTopic + "\", \"uniq_id\": \"" + id + "\", \"tilt-min\": 0, \"tilt-max\": 100, \"tilt_clsd_val\": 0, \"tilt_opnd_val\": 33, " + dev + " }";
  mqtt.publish(topic.c_str(), config.c_str(), true);
}

void mqttReconnect()
{
  static long lastReconnect = 0;

  if (millis() - lastReconnect > 5000)
  {
    mqtt.setBufferSize(512);

    String availTopic;
    availTopic = "blinds/" + mdnsName + "/availability";

    if (mqtt.connect(mdnsName.c_str(), mqttUser.c_str(), mqttPassword.c_str(), availTopic.c_str(), 0, true, "offline"))
    {
      mqtt.publish(availTopic.c_str(), "online", true);

      String topic = "blinds/" + mdnsName + "/command";
      mqtt.subscribe(topic.c_str());
      
      topic = "blinds/" + mdnsName + "/tilt";
      mqtt.subscribe(topic.c_str());
      
      MQTT_haDiscovery();
      mqttSendUpdate();
      lastReconnect = 0;
    }
    else lastReconnect = millis();
  }
}

void mqttCallback(char* topic, byte* payload, unsigned int length)
{
  if (!isCalibrated) return;

  if (strstr(topic, "tilt"))
  {
    uint8_t percent = atoi((char*)payload);
    if (percent == 0) goHome();
    else targetStepCount = (fullStepCount * percent) / 100;

    Serial.print("MQTT Tilt command received: ");
    Serial.print(percent);
    Serial.print("%, ");
    Serial.println(targetStepCount);
  }
  else if (strstr(topic, "command"))
  {
    if (!strncasecmp_P((char*)payload, "open", length)) targetStepCount = fullStepCount / 3;
    else if (!strncasecmp_P((char*)payload, "close", length)) goHome();
    else if (!strncasecmp_P((char*)payload, "stop", length)) targetStepCount = currentStepCount;

    Serial.print("MQTT command received: ");
    Serial.println((char*)payload);
  }

  mqttSendUpdate();
}

void mqttSendUpdate()
{
  char percentChar[4];
  if (fullStepCount > 0) itoa((targetStepCount * 100) / fullStepCount, percentChar, 10);
  else itoa(0, percentChar, 10);
  String pubTopic = "blinds/" + mdnsName + "/tilt-state";
  mqtt.publish(pubTopic.c_str(), percentChar, true);
  
  pubTopic = "blinds/" + mdnsName + "/state";
  mqtt.publish(pubTopic.c_str(), (targetStepCount > 0 ? "open" : "closed"), true);
}

void initEEPROMSettings()
{
  EEPROM.write(SETTING_INITIALIZED, SETTINGS_REV);  // EEPROM initialized
  EEPROM.write(SETTING_MOTOR_INVERT,           0);
  EEPROM.write(SETTING_DELIM,                  0);  // Dummy setting to delimit start of strings
  writeEEPROMString(SETTING_MDNS, SETTING_MDNS_LENGTH, mdnsName);
  writeEEPROMString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH, "");
  writeEEPROMString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH, "");
  writeEEPROMString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH, "");
  EEPROM.commit();
}

void readEEPROMSettings()
{
  if (EEPROM.read(SETTING_INITIALIZED) != SETTINGS_REV) initEEPROMSettings();

  motorInvert = EEPROM.read(SETTING_MOTOR_INVERT);
  mdnsName = readEEPROMString(SETTING_MDNS, SETTING_MDNS_LENGTH);
  mqttServer = readEEPROMString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH);
  mqttUser = readEEPROMString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH);
  mqttPassword = readEEPROMString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH);
}

String readEEPROMString(uint16_t startAddress, uint16_t lengthAddress)
{
  String returnString = "";
  uint8_t length = constrain(EEPROM.read(lengthAddress), 0, MAX_STRING_LENGTH);
  for (int i = 0; i < length; i++) returnString += (char)EEPROM.read(startAddress + i);
  return returnString;
}

void writeEEPROMString(uint16_t startAddress, uint16_t lengthAddress, String str)
{
  uint8_t length = constrain(str.length(), 0, MAX_STRING_LENGTH);
  EEPROM.write(lengthAddress, length);
  for (int i = 0; i < length; i++) EEPROM.write(startAddress + i, str[i]);
  EEPROM.commit();
}

void getSettings()
{
  String response;
  IPAddress ip = WiFi.localIP();

  response = mdnsName;
  response += ",";
  response += mqtt.connected();
  response += ",";
  response += String(ip[0]) + "." + String(ip[1]) + "." + String(ip[2]) + "." + String(ip[3]);
  response += ",";
  response += mqttServer;
  response += ",";
  response += mqttUser;
  response += ",";
  response += mqttPassword;
  response += ",";
  response += (motorInvert ? "1" : "0");
  response += ",";
  response += fullStepCount;

  server.sendHeader("Cache-Control", "no-cache");
  server.send(200, "text/plain", response);
}

void saveSettings()
{
  String response;

  if (server.hasArg("motorInvert"))
  {
    motorInvert = (server.arg("motorInvert").toInt() ? true : false);
    EEPROM.write(SETTING_MOTOR_INVERT, motorInvert);
  }
  if (server.hasArg("mdnsName"))
  {
    mdnsName = server.arg("mdnsName");
    writeEEPROMString(SETTING_MDNS, SETTING_MDNS_LENGTH, mdnsName);
  }
  if (server.hasArg("mqttServer"))
  {
    mqttServer = server.arg("mqttServer");
    writeEEPROMString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH, mqttServer);
  }
  if (server.hasArg("mqttUser"))
  {
    mqttUser = server.arg("mqttUser");
    writeEEPROMString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH, mqttUser);
  }
  if (server.hasArg("mqttPassword"))
  {
    mqttPassword = server.arg("mqttPassword");
    writeEEPROMString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH, mqttPassword);
  }
  EEPROM.commit();

  response = F("<META http-equiv=\"refresh\" content=\"10;URL='/'\">Settings saved! Restarting to take effect...<br><br>");
  server.send(200, "text/html", response);
  delay(1000);
  ESP.reset();
}

bool handleFileRead(String path)
{
  if (path.endsWith("/")) path += "index.htm";

  String dataType = "text/plain";
  if (path.endsWith(".htm")) dataType = "text/html";
  else if (path.endsWith(".css")) dataType = "text/css";

  if (SPIFFS.exists(path))
  {
    File file = SPIFFS.open(path, "r");
    server.streamFile(file, dataType);
    file.close();
    return true;
  }
  return false;
}